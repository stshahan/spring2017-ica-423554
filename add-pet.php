<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="stylesheetICA.css">
<title>Add Pet Listing</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<!-- CONTENT HERE -->
<h1> Add New Pet </h1>
<p>Enter some info about your pet</p> 
<a href="pet-login.php">Login</a>
<a href="pet-listings.php">Pet Listings</a>
<form name="petty" action="pet-submit.php" enctype="multipart/form-data">
Name
<input type="text" name="petname"/>
<input name="username" type="hidden" />
Species
<select name="species">
<option value="cat">cat</option>
<option value="dog">dog</option>
<option value="chinchilla">chinchilla</option>
<option value="snake">snake</option>
<option value="rabbit">rabbit</option>
</select>
Weight
<input type="number" name="weight" />
Description
<textarea name="description">
</textarea>
<input type="file" name="picture"/>
<input type="submit" value="submit"/>
</form>

</div></body>
</html>
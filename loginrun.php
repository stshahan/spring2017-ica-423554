<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="stylesheetICA.css">
<title>Add Pet Listing</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<!-- taken almost directly from my mod 3 work -->
<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.
 session_start();
require 'alwaysinclude.php';
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
 
// Bind the parameter
$user = $_POST['username'];
$stmt->bind_param('s', $user);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = $_POST['psw'];
// Compare the submitted password to the actual password hash
// In PHP < 5.5, use the insecure: if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
 
if($cnt == 1 && password_verify($pwd_guess, $pwd_hash)){
	// Login succeeded!
	$_SESSION['user_id'] = $user_id;
	$_SESSION['username']=(string) $user;
	header("Location: add-pet.php");
	exit;
	// Redirect to your target page
} else{
	// Login failed; redirect back to the login screen
	echo $cnt;
	echo password_verify($pwd_guess, $pwd_hash);
	header("Location: pet-login.php");
	exit;
}
?>

</div></body>
</html>
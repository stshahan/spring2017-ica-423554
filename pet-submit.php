<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="stylesheetICA.css">
<title>Add Pet Listing</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<!-- Most of PHP taken from my mod 3 work -->
<h1> Add New Pet </h1>
<p>Enter some info about your pet</p> 
<a href="pet-login.php">Login</a>
<a href="pet-listings.php">Pet Listings</a>
<?php
if ($_POST['petname']==NULL){
	printf("error");
	header("Location: add-pet.php");
}

$petname=(string) $_POST['petname'];
$species=(string)$_POST['species'];
$weight=(float) $_POST['weight'];
$description=(string)$_POST['description'];
$picture=(string)$_POST['picture'];

require('alwaysinclude.php');
if(0==0){
$stmt=$mysqli ->prepare("insert into pets(username, species, name, filename, weight, description) values(?,?,?,?,?,?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ssssss', $username, $species, $petname, $picture, $weight, $description);
 
$stmt->execute();
 
$stmt->close();
}
header("Location: add-pet.php");
exit;
 

?>

</div></body>
</html>
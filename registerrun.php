<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" type="text/css" href="stylesheetICA.css">
<title>Add Pet Listing</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<!-- CONTENT HERE -->
<h1> Login!</h1>
<!-- This is taken almost directly from my module 3 homework -->
<?php
require('alwaysinclude.php');
$userna=(string) $_POST['username'];
$passwo=(string) $_POST['psw'];
$salt=(string) password_hash($passwo, PASSWORD_DEFAULT);
if(0==0){
$stmt=$mysqli ->prepare("insert into users(username, password) values(?,?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ss', $userna, $salt);
 
$stmt->execute();
 
$stmt->close();
}
header("Location: add-pet.php");
exit;
 
?>

</div></body>
</html>